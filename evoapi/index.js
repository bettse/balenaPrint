require('dotenv').config()
const os = require("os");
const { spawn, execSync } = require("child_process");
const { join } = require('path')
const { NFC } = require("nfc-pcsc");
const NDEF = require("ndef");
const express = require('express')
const bodyParser = require('body-parser')
const winston = require('winston');
const expressWinston = require('express-winston');

const { CL_READER } = process.env

const PORT = 8080
const COMM_ERROR = "ERROR: evo_write_usb_port - write error (sync)\nERROR: fail to write\n"
const uidAsciiLength = 14;
const counterAsciiLength = 6;
const NOT_FOUND = -1;

const nfc = new NFC();
const app = express()
app.use(bodyParser.json())
const readers = {};

app.use(expressWinston.logger({
	transports: [
		new winston.transports.Console()
	],
	meta: false,
	format: winston.format.combine(
		winston.format.json()
	),
	expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
	colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
}));

const device = execSync("readlink -f /dev/evolis").toString().replace('\n', '');
const noop = () => {};

async function sendCommand(cmd) {
  // TODO: Detect arch and use correct version
  const process = spawn("./evocomYocto", ['-d', device, cmd]);
  return new Promise((resolve, reject) => {
    const output = [];

    process.stdout.on("data", data => {
      output.push(data.toString())
    });

    process.stderr.on("data", data => {
      output.push(data.toString())
    });

    process.on('error', (error) => {
      reject(error);
    });

    process.on("close", code => {
      resolve(output.join('').replace('\n', ''));
    });
  });
}

async function sendCommandWithRetry(cmd, retries = 3) {
  let output = '';
  do {
    output = await sendCommand(cmd);
  } while (output == COMM_ERROR && retries-- > 0);
  if (output !== 'OK') {
    throw 'failed ot run command';
  }

  return {
    output,
    remainingRetries: retries,
  }
}

app.get('/', (req, res) => {
	res.status(200).send("Hello world")
})

app.post('/evoapi', async (req, res) => {
  let retries = 3;
	const { cmd, url } = req.body;
  if (cmd) {
    sendCommandWithRetry(cmd);
  } else if (url) {
    const reader = readers[CL_READER];
    if (!reader) {
      return res.status(422).send({error: 'no reader found'});
    }

    if (os.hostname().split('.').shift() !== 'ataraxia') {
      const { output, retries } = sendCommandWithRetry('Sic');
      console.log({output, retries})
    }
    const { card } = reader;
    if (!card) {
      return res.status(422).send({error: 'no card detected by reader'});
    }
    if (!isNtag(card)) {
      return res.status(422).send({error: 'not an ntag'});
    }

    const { uid } = card;
    const {ndef, uidMirrorOffset, counterMirrorOffset} = genetateNDEF(url);
    await writeNdef(reader, ndef);
    await writeMirrors(reader, {uidMirrorOffset, counterMirrorOffset});

    if (os.hostname().split('.').shift() !== 'ataraxia') {
      const { output, retries } = sendCommandWithRetry('Si;B');
      console.log({output, retries})
    }

    return res.status(200).send({
      uid,
      ndef: ndef.toString('hex'),
    });
  }
})

function isNtag(card) {
  //https://flomio.com/forums/topic/list-of-apdu-codes/#post-20335
  const {atr} = card;

  const cardName = atr.readUint16BE(13)
  switch(cardName) {
    case 0x0001: //Mifare 1k
      console.log("Mifare 1k");
      break;
    case 0x0002: //Mifare 4k
      console.log("Mifare 3k");
      break;
    case 0x0003: //Mifare Ultralight
      console.log("Mifare Ultralight (NTAG)");
      return true;
    case 0x0026: //Mifare Mini
      console.log("Mifare Mini");
      break;
    case 0xF004: //Topaz and Jewel
      console.log("Topaz/Jewel");
      break;
    case 0xF011: // Felica 212k
      console.log("Felica 212k");
      break;
    case 0xF012: // Felica 424k
      console.log("Felica 424k");
    default:
      console.log('unknown cardName', cardName);
      break;
  }
  return false;
}

function genetateNDEF(url) {
  url = url.replace("{uid}", 'U'.repeat(uidAsciiLength));
  url = url.replace("{counter}", 'C'.repeat(counterAsciiLength));

  const message = [NDEF.uriRecord(url)];

  const bytes = Buffer.from(NDEF.encodeMessage(message));
  if (bytes.length >= 255) {
    throw new Error("No support for ndef > 255 yet");
  }

  // Maybe do repalcement/indexof here?
  const buffer = Buffer.alloc(3 + bytes.length);
  buffer[0] = 0x03; // NDEF TLV tag
  buffer[1] = bytes.length;
  bytes.copy(buffer, 2);
  buffer[buffer.length - 1] = 0xfe; // Terminator TLV tag

  let uidMirrorOffset = buffer.indexOf('U'.repeat(uidAsciiLength));
  let counterMirrorOffset = buffer.indexOf('C'.repeat(counterAsciiLength));
  if (uidMirrorOffset !== NOT_FOUND) {
    buffer.fill('0', uidMirrorOffset, uidMirrorOffset + uidAsciiLength);
  }
  if (counterMirrorOffset !== NOT_FOUND) {
    buffer.fill('0', counterMirrorOffset, counterMirrorOffset + counterAsciiLength);
  }

  //NTAG 21x limit the uid/counter mirroring in a number of ways
  if (uidMirrorOffset !== NOT_FOUND && counterMirrorOffset !== NOT_FOUND) {
    if (uidMirrorOffset > counterMirrorOffset) {
      throw new Error(`Counter mirror must come after UID mirror (${uidMirrorOffset} > ${counterMirrorOffset})`);
    }
    if (counterMirrorOffset !== (uidMirrorOffset + uidAsciiLength + 1)) {
      throw new Error(`Counter mirror must be one character after UID mirror end`);
    }
  }

  const results = {
    ndef: buffer,
  }
  if (uidMirrorOffset !== NOT_FOUND) {
    results["uidMirrorOffset"] = uidMirrorOffset;
  }

  if (counterMirrorOffset !== NOT_FOUND) {
    results["counterMirrorOffset"] = counterMirrorOffset;
  }
  return results;
}

async function writeNdef(reader, data) {
  const startingPage = 4;
  const pageLength = 4;

  const CC = await reader.read(3, pageLength, pageLength, pageLength);
  if (CC[0] != 0xe1) {
    throw new Error("Magic byte for NDEF missing");
  }

  const size = CC[2];
  let lastUserPage = 0;
  switch (size) {
    case 0x12:
      lastUserPage = 0x27;
      break;
    case 0x3e:
      lastUserPage = 0x81;
      break;
    case 0x6d:
      lastUserPage = 0xe1;
      break;
  }

  for (var page = startingPage; page <= lastUserPage; page++) {
    const start = (page - startingPage) * pageLength;
    const end = Math.min(start + pageLength, data.length);
    const pad = Buffer.alloc(pageLength - (end - start));
    const pageData = Buffer.concat([data.slice(start, end), pad]);
    if (pad.length < pageLength) {
      await reader.write(page, pageData);
    }
  }
}

async function writeMirrors(reader, {uidMirrorOffset, counterMirrorOffset}) {
  const pageLength = 4;
  const CC = await reader.read(3, pageLength, pageLength, pageLength);

  const size = CC[2];
  let configPageNum = 0;
  switch (size) {
    case 0x12:
      configPageNum = 0x29;
      break;
    case 0x3e:
      configPageNum = 0x83;
      break;
    case 0x6d:
      configPageNum = 0xe3;
      break;
  }
  const configPage = await reader.read(configPageNum, pageLength, pageLength, pageLength);

  const MIRROR = configPage[0];
  const MIRROR_PAGE = configPage[2];
  const MIRROR_CONF = (MIRROR & 0xC0) >> 6;
  const MIRROR_BYTE = (MIRROR & 0x30) >> 4;

  let new_mirror_conf = 0x00;
  if (uidMirrorOffset) {
    new_mirror_conf = new_mirror_conf | 0x01
  }
  if (counterMirrorOffset) {
    new_mirror_conf = new_mirror_conf | 0x10;
  }

  const new_mirror_page = 4 + Math.floor(uidMirrorOffset / 4)
  const new_mirror_byte = uidMirrorOffset % 4
  const new_mirror = (MIRROR & 0x0F) | (new_mirror_conf << 6) | (new_mirror_byte << 4)
  const new_config = Buffer.from([new_mirror, configPage[1], new_mirror_page, configPage[3]])

  console.log({old: {configPage, MIRROR_PAGE, MIRROR_CONF, MIRROR_BYTE}, new: {new_mirror_conf, new_mirror_page, new_mirror_byte, new_mirror, new_config}})
  if (!new_config.equals(configPage)) {
    await reader.write(configPageNum, new_config);
  }
}

nfc.on("reader", async reader => {
  console.log(reader.reader.name)
  readers[reader.reader.name] = reader;

  reader.on('card.off', card => {
    console.log(`${reader.reader.name} card removed`, card);
  });

  reader.on("error", err => {
    console.error(`an error occurred`, reader, err);
  });

  reader.on("end", () => {
    console.info(`device removed`, reader);
  });
});

nfc.on("error", err => {
  console.error(`an error occurred`, err);
});

// start a server on port 80 and log its start to our console
app.listen(PORT, () => {
	console.log(`App listening on port ${PORT}`)
})
