#!/bin/bash

OS_VERSION=$(echo "$BALENA_HOST_OS_VERSION" | cut -d " " -f 2)
echo "OS Version is $OS_VERSION"

pushd output
mod_dir="usblp_${BALENA_DEVICE_TYPE}_${OS_VERSION}*"
for each in $mod_dir; do
	echo Loading module from "$each"
	insmod "$each/usblp.ko"
done
popd

pushd ccid-1.4.36
make install
popd

/usr/sbin/pcscd --force-reader-polling

# Sometimes you'll fail
node index
